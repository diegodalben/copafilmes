﻿using System;
using System.Collections.Generic;
using CopaFilmes.BizLogic.Entities;
using CopaFilmes.BizLogic.Repositories.Abstraction;
using CopaFilmes.Infrastructure.HttpClient.Abstraction;
using Newtonsoft.Json;

namespace CopaFilmes.DataAccess
{
    public sealed class MovieAzureApi : IMovieRepository
    {
        private const string MovieApiUri = "https://copa-filmes.azurewebsites.net/api/filmes";
        private readonly IHttpHandler _httpHandler;

        public MovieAzureApi(IHttpHandler httpHandler)
        {
            _httpHandler = httpHandler ?? throw new ArgumentNullException();
        }

        public IList<Movie> GetMovies()
        {
            var response = _httpHandler.GetStringAsync(MovieApiUri).Result;
            return JsonConvert.DeserializeObject<IList<Movie>>(response);
        }
    }
}